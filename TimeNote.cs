using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON
{
    class TimeNote : Note 
    {
       private DateTime time ;
       
        public TimeNote() :base() { this.time = DateTime.Now; }
        public TimeNote(String name, String text, int importance): base(name, text, importance) { 
            time = DateTime.Now; 
        }

         public DateTime Time
        {

            get { return this.time; }
            set { this.time = value; }


        }
        public override string ToString()
        {
            return base.ToString() + " " + this.time;
        }

    }
}
