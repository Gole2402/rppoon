using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON
{
    class Note
    {
        private String name;
        private String text;        
        private int importance;

        int getImportance() { return importance; }
        void setImportance(int value) { importance = value; }

        public int Importance
        {
            get { return importance; }
            set { this.importance = value; }                
        }

        public String Text
        {
            get { return text; }
            set { this.text = value; }
        }

        public String Name
        {
            get { return name; }
            private set { this.name = value; }
        }
        public Note(){
            this.name = "Unknown";
            this.text = "Nothing";
            this.importance = 0;}
        public Note(String name,String text,int importance) { 
            this.name = name;
            this.text = text;
            this.importance = importance; }
        public Note(String name,String text) {
            this.name = name;
            this.text = text; }

        public override string ToString()
        {
            return (this.name + " "+ this.importance + " " +this.text) ;
        }


    }
}
