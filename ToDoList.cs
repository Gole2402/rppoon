using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPPOON;


namespace RPPOON
{
    class ToDoList
    {
        private List<Note> tasks ;
        
        public ToDoList()
        {
            this.tasks = new List<Note>();
        }

        public ToDoList(List<Note> tasks)
        {
            this.tasks = tasks;     
        }

        public void AddTask(Note note)
        {
            tasks.Add(note);
        }

        public void DoTask()
        {
            int highestpriority = findHighestPriority();
            for(int i=0;i<tasks.Count; i++)
            {
                if (tasks[i].Importance == highestpriority)
                {
                    tasks.RemoveAt(i);
                }
            }
        }

        public int findHighestPriority()
        {
            int highest = 0;
            foreach(Note note in tasks)
            {
                if (note.Importance>highest)
                {
                    highest = note.Importance;
                }
            }
            return highest;
        }

        public void printList()
        {
            foreach(Note note in tasks)
            {
                Console.WriteLine(note.ToString()); 
            }
        }
        public Note getNote (int index)
        {
            if (index >= tasks.Count)
            {
                throw new IndexOutOfRangeException("ERROR!! ...Ne postoji element na tom indeksu!!");
            }
            else
            {
                return tasks[index];
            }
        }
    }
}
