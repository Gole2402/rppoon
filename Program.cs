using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note = new Note();
            Note first = new Note("Ivan", "Trgovina", 5);
            Note second = new Note("Petar", "Trening");      
            Console.WriteLine(note.Name);
            Console.WriteLine(second.Name);
            Console.WriteLine(second.Text);
            Console.WriteLine(second.Importance);
            Console.WriteLine(first.ToString());
            Console.WriteLine(first);

            TimeNote third = new TimeNote("Dino", "Trening", 7);

            ToDoList tasks = new ToDoList();

            Console.WriteLine(third);

            Console.WriteLine("Unesi broj zadataka: ");
            int number = Convert.ToInt32(Console.ReadLine());
            for ( int i =0;i<number;i++)
            {
                Console.WriteLine("Unesi ime: ");
                string name = Console.ReadLine();
                Console.WriteLine("Unesi zadatak: ");
                string task = Console.ReadLine();
                Console.WriteLine("Unesi prioritet: ");
                int pritority = Convert.ToInt32(Console.ReadLine());
                Note note1 = new Note(name, task, pritority);
                tasks.AddTask(note1);

            }

            tasks.printList();
            tasks.DoTask();
            Console.WriteLine();
            tasks.printList();
            Console.ReadLine();




        }
    }
}


